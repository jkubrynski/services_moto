package com.moto.micro.auction;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auctions")
class AuctionController {

	private final AuctionService auctionService;

	AuctionController(AuctionService auctionService) {
		this.auctionService = auctionService;
	}

	@GetMapping("/{id}")
	Payment getAuction(@PathVariable String id) {
		return auctionService.getAuctionDetails(id);
	}

}
