package com.moto.micro.auction;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class AuctionService {

	private final PaymentClient paymentClient;

	AuctionService(PaymentClient paymentClient) {
		this.paymentClient = paymentClient;
	}


	Payment getAuctionDetails(String id) {
		return paymentClient.getPaymentStatus(id);
	}
}
