package com.moto.micro.auction;

class Payment {

	private String id;
	private String status;
	private int paymentAmount;

	public Payment() {
	}

	Payment(String id, String status, int paymentAmount) {
		this.id = id;
		this.status = status;
		this.paymentAmount = paymentAmount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(int paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
}
