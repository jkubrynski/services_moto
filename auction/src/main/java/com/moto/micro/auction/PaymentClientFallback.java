package com.moto.micro.auction;

import org.springframework.stereotype.Component;

@Component
class PaymentClientFallback implements PaymentClient {

	@Override
	public Payment getPaymentStatus(String id) {
		return new Payment(id, "UNKNOWN", -1);
	}
}
