package com.moto.micro.auction;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "payment", fallback = PaymentClientFallback.class)
interface PaymentClient {

	@RequestMapping(value = "/payments/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Payment getPaymentStatus(@PathVariable("id") String id);
}
