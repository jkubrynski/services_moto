package com.moto.micro.auction;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = "com.moto.micro:payment:+:stubs")
@ActiveProfiles("contracts")
public class PaymentClientTest {

	@Autowired
	PaymentClient paymentClient;

	@Test
	public void shouldReturnPaymentDetails() {
		Payment payment = paymentClient.getPaymentStatus("2");

		assertThat(payment.getId()).isEqualTo("2");
		assertThat(payment.getStatus()).isEqualTo("PAID");
		assertThat(payment.getPaymentAmount()).isEqualTo(20);
	}
}

