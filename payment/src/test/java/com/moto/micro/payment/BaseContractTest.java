package com.moto.micro.payment;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.mockito.Mockito;

abstract class BaseContractTest {

	@Before
	public void setUp() {
		PaymentService paymentService = Mockito.mock(PaymentService.class);
		Mockito.when(paymentService.getPayment("2")).thenReturn(new Payment("2", PaymentStatus.PAID, 20));
		RestAssuredMockMvc.standaloneSetup(new PaymentController(paymentService));
	}
}
