import org.springframework.cloud.contract.spec.Contract

Contract.make {
	request {
		method(GET())
		url("/payments/2")
		headers {
			accept(applicationJsonUtf8())
		}
	}
	response {
		status(OK())
		headers {
			contentType(applicationJsonUtf8())
		}
		body(
				"id": "2",
				"status": "PAID",
				"paymentAmount": 20
		)
	}
}
