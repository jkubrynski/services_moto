package com.moto.micro.payment;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
class PaymentService {

	Payment getPayment(@PathVariable String id) {
		return new Payment(id, PaymentStatus.PAID, 10);
	}
}
