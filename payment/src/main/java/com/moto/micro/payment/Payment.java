package com.moto.micro.payment;

public class Payment {
	private String id;
	private PaymentStatus status;
	private int amount;

	Payment(String id, PaymentStatus status, int amount) {
		this.id = id;
		this.status = status;
		this.amount = amount;
	}

	public String getId() {
		return id;
	}


	public PaymentStatus getStatus() {
		return status;
	}

	public int getAmount() {
		return amount;
	}

	public int getPaymentAmount() {
		return amount;
	}
}
